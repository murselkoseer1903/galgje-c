﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Galgje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string HetWoord;
        private string JuisteLetters;
        private int levens;
        private string FouteLetters;
        private int i = 1;
        private string VerborgenWoord;
        private double seconden = 10;
       
        
        DispatcherTimer tijd = new DispatcherTimer();







        public MainWindow()
        {
            InitializeComponent();
            Text_Uitkomst.Text = "Geef een geheim woord";
            





        }

        private void Button_Verberg_Woord_Click(object sender, RoutedEventArgs e)
        {
            
            timer();
            VerbergSpel();


        }

        private void Button_Nieuwe_Spel_Click(object sender, RoutedEventArgs e)
        {
            i = 1;
            NieuwSpel();
            
            Button_Verberg_Woord.Visibility = Visibility.Visible;
            Button_Raad.IsEnabled = false;
            fotos.Source = new BitmapImage(new Uri(@"Foto/1.jpg", UriKind.Relative));
        }

        private void HetWoordRaden()
        {
            
            string HetLetter = Tekst_Ingeven.Text;
            bool Poging = HetWoord.Contains(HetLetter);

            
           
            if (Poging && HetLetter.Length == HetWoord.Length)
            {
                Text_Uitkomst.Text = $"Je hebt {HetWoord} correct geraden";

            }
            else if (HetLetter.Length > 1)
            {
                --levens;
                Text_Uitkomst.Text = $"{levens} Levens \n" +
                $"Juiste Letters: {JuisteLetters} \n" +
                $"Foute Letters: {FouteLetters} \n" +
                $"{VerborgenWoord}";
                Tekst_Ingeven.Text = string.Empty;
                fotos.Source = new BitmapImage(new Uri(@"Foto/" + i + ".jpg", UriKind.Relative));
            }
            else if (Poging)
            {
                
                JuisteLetters = JuisteLetters + $"{HetLetter}";
                VerborgenWoord = "";

                foreach (char geradenletter in HetWoord)
                {
                    if (JuisteLetters.Contains(geradenletter))
                    {
                        VerborgenWoord = VerborgenWoord + geradenletter;
                    }
                    else
                    {
                        VerborgenWoord = VerborgenWoord + "*";
                    }

                }
                Text_Uitkomst.Text = $"{levens} Levens \n" +
                $"Juiste Letters: {JuisteLetters} \n" +
                $"Foute Letters: {FouteLetters} \n" +
                $"{VerborgenWoord}";

                Tekst_Ingeven.Text = string.Empty;

                
                

            }
            
            else if (Poging == false)
            {
                VulWoord();
                i++;
                FouteLetters = FouteLetters + $"{HetLetter}";
                --levens;
                Text_Uitkomst.Text = $"{levens} Levens \n" +
                $"Juiste Letters: {JuisteLetters} \n" +
                $"Foute Letters: {FouteLetters} \n" +
                $"{VerborgenWoord}";
                Tekst_Ingeven.Text = string.Empty;
                fotos.Source = new BitmapImage(new Uri(@"Foto/" + i + ".jpg", UriKind.Relative));
                
            }
            if (levens == 0)
            {
                Button_Raad.IsEnabled = false;
                Text_Uitkomst.Text = $"Je hebt geen levens meer,\n het woord was {HetWoord}";

            }

            
            
            
       
            
        }

        private void VulWoord()
        {
            VerborgenWoord = "";

            foreach (char geradenletter in HetWoord)
            {
                if (JuisteLetters.Contains(geradenletter))
                {
                    VerborgenWoord = VerborgenWoord + geradenletter;
                }
                else
                {
                    VerborgenWoord = VerborgenWoord + "*";
                }

            }
        }
        

        private void Button_Raad_Click(object sender, RoutedEventArgs e)
        {
            seconden = 10;
            
            HetWoordRaden();
        }

        private void VerbergSpel()
        {
            

            levens = 10;
            HetWoord = Tekst_Ingeven.Text;
            Button_Verberg_Woord.Visibility = Visibility.Hidden;
            Button_Raad.IsEnabled = true;

            
            JuisteLetters = "";
            FouteLetters = "";
            Text_Uitkomst.Text = $"{levens} Levens \n" +
                $"Juiste Letters: {JuisteLetters} \n" +
                $"Foute Letters: {FouteLetters} \n" +
                $"{VerborgenWoord}";


            Tekst_Ingeven.Text = "";
            
        }

        private void NieuwSpel()
        {
            Text_Uitkomst.Text = "Geef een geheim woord";
            HetWoord = String.Empty;
            JuisteLetters = String.Empty;
            FouteLetters = String.Empty;
            Tekst_Ingeven.Text = String.Empty;
            VerborgenWoord = String.Empty;  
        }

        
        private void timer()
        {
            tijd.Interval = TimeSpan.FromSeconds(1);
            tijd.Tick += Timer_Tick;
            tijd.Start();
 
        }
        
        private void Timer_Tick(object? sender, EventArgs e)
        {
            seconden--;
            Text_Tijd.Text = $"{seconden.ToString()} Seconden";
            if (seconden == 0)
            {
                tijd.Stop();
                Text_Tijd.Text = "Tijd is op!";
                levens--;
                i++;
                fotos.Source = new BitmapImage(new Uri(@"Foto/" + i + ".jpg", UriKind.Relative));
                seconden = 10;
                tijd.Start();
                Text_Uitkomst.Text = $"{levens} Levens \n" +
                $"Juiste Letters: {JuisteLetters} \n" +
                $"Foute Letters: {FouteLetters} \n" +
                $"{VerborgenWoord}";
            }
            
            


        }

    }



    
}
